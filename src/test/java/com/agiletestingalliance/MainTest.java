package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MainTest {

    @Test
    public void aboutTest() throws Exception {
        String res = new AboutCPDOF().desc();
        assertTrue("Description", res.contains("certification"));
    }

    @Test
    public void durationTest() throws Exception {
        String res = new Duration().getDuration();
        assertTrue("Duration", res.contains("working professionals"));
    }


    @Test
    public void minMaxFindBiggestTest() throws Exception {
        int res = new MinMax().findBiggestNo(5,10);
        assertEquals("Biggest B", 10, res);

        int resA = new MinMax().findBiggestNo(20,10);
        assertEquals("Biggest A", 20, resA);
    }

    @Test
    public void barTest() throws Exception {
        String res = new MinMax().bar(null);
        assertEquals("String null", null, res);

        String resA = new MinMax().bar("");
        assertEquals("String empty", "", resA);

        String resB = new MinMax().bar("test");
        assertEquals("String test", "test", resB);
    }

    @Test
    public void testClassTest() throws Exception {
        TestClass res = new TestClass("Petros");
        assertEquals("CheckString", "Petros", res.getStringA());

    }

    @Test
    public void usefulnessTest() throws Exception {
        String res = new Usefulness().desc();
        assertTrue("Desc", res.contains("about transformation"));

        Usefulness useful = new Usefulness();
        useful.functionWF();
    }

}
